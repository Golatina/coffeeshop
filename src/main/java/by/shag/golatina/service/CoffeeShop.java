package by.shag.golatina.service;

import by.shag.golatina.exception.CountryNotFoundException;
import by.shag.golatina.model.Coffee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoffeeShop {

    @Autowired
    private ItalianCoffeeShop italianCoffeeShop;

    @Autowired
    private SpanishCoffeeShop spanishCoffeeShop;

    @Autowired
    private GermanCoffeeShop germanCoffeeShop;

    public List<Coffee> makeCoffee(CountryList country, CoffeeType coffeeType) {
        List<Coffee> coffeeList;
        if (country == CountryList.SPAIN) {
            coffeeList = spanishCoffeeShop.getByType(coffeeType);
        } else if (country == CountryList.GERMANY) {
            coffeeList = germanCoffeeShop.getByType(coffeeType);
        } else if (country == CountryList.ITALY) {
            coffeeList = italianCoffeeShop.getByType(coffeeType);
        } else {
            throw new CountryNotFoundException(country.name());
        }
        return coffeeList;
    }

}
