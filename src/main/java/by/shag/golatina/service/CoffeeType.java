package by.shag.golatina.service;

public enum CoffeeType {

    ARABICA, ROBUSTA, LIBERICA, NEW_NONAME
}

