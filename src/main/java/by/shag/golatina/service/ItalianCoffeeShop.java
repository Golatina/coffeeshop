package by.shag.golatina.service;

import by.shag.golatina.exception.CoffeeNotFoundException;
import by.shag.golatina.model.Arabica;
import by.shag.golatina.model.Coffee;
import by.shag.golatina.model.Liberica;
import by.shag.golatina.model.Robusta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItalianCoffeeShop {

    @Autowired
    @Qualifier("italianArabica1")
    private Arabica arabica1;

    @Autowired
    @Qualifier("italianArabica2")
    private Arabica arabica2;

    @Autowired
    @Qualifier("italianRobusta1")
    private Robusta robusta1;

    @Autowired
    @Qualifier("italianRobusta2")
    private Robusta robusta2;

    @Autowired
    @Qualifier("italianLiberica1")
    private Liberica liberica1;

    @Autowired
    @Qualifier("italianLiberica2")
    private Liberica liberica2;

    public List<Coffee> getByType(CoffeeType coffeeType) {
        List<Coffee> coffeeList = new ArrayList<>();
        if (coffeeType == CoffeeType.ARABICA) {
            coffeeList.add(arabica1);
            coffeeList.add(arabica2);
        } else if (coffeeType == CoffeeType.ROBUSTA) {
            coffeeList.add(robusta1);
            coffeeList.add(robusta2);
        } else if (coffeeType == CoffeeType.LIBERICA) {
            coffeeList.add(liberica1);
            coffeeList.add(liberica2);
        } else {
            throw new CoffeeNotFoundException(getClass().getSimpleName(), coffeeType.name());
        }
        return coffeeList;
    }
}
