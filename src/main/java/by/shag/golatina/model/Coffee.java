package by.shag.golatina.model;

public abstract class Coffee {

    private String country;

    private Boolean bitter;

    private String name;

    private Integer weight;

    public String getReceipt() {
        return "country='" + country + '\'' +
                ", name='" + name + '\'' +
                ", bitter='" + bitter + '\'' +
                ", weight=" + weight;
    }

    public Coffee(String country, String name, Integer weight, Boolean bitter) {
        setCountry(country);
        setName(name);
        setWeight(weight);
        setBitter(bitter);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Boolean getBitter() {
        return bitter;
    }

    public void setBitter(Boolean bitter) {
        this.bitter = bitter;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Coffee{" +
                "country='" + country + '\'' +
                ", bitter=" + bitter +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
