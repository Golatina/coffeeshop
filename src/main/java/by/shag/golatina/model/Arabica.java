package by.shag.golatina.model;

public class Arabica extends Coffee {

    public Arabica(String country, String name, Integer weight) {
        super(country, name, weight, false);
    }

    @Override
    public String toString() {
        return "Arabica "  + super.toString();
    }

}
