package by.shag.golatina.model;

public class Liberica extends Coffee {

    public Liberica(String country, String name, Integer weight) {
        super(country, name, weight, true);
    }

    @Override
    public String toString() {
        return "Liberica "  + super.toString();
    }

}
