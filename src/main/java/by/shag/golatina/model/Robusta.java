package by.shag.golatina.model;

public class Robusta extends Coffee {

    public Robusta(String country, String name, Integer weight) {
        super(country, name, weight, true);
    }

    @Override
    public String toString() {
        return "Robusta "  + super.toString();
    }

}
