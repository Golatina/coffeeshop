package by.shag.golatina;

import by.shag.golatina.service.CoffeeShop;
import by.shag.golatina.service.CoffeeType;
import by.shag.golatina.service.CountryList;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

@SpringBootApplication
public class Runner implements ApplicationContextAware {

    private static ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(Runner.class);

        context.getBean(CoffeeShop.class).makeCoffee(CountryList.GERMANY, CoffeeType.ROBUSTA).forEach(System.out::println);
        context.getBean(CoffeeShop.class).makeCoffee(CountryList.CHINA, CoffeeType.ARABICA).forEach(System.out::println);
        context.getBean(CoffeeShop.class).makeCoffee(CountryList.SPAIN, CoffeeType.NEW_NONAME).forEach(System.out::println);

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
