package by.shag.golatina.bean;

import by.shag.golatina.model.Arabica;
import by.shag.golatina.model.Liberica;
import by.shag.golatina.model.Robusta;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansFactory {

    @Value("${italian.country}")
    String italianCountry;

    @Value("${spanish.country}")
    String spanishCountry;

    @Value("${german.country}")
    String germanCountry;

    @Bean
    public Arabica italianArabica2(@Value("${italian.arabica.weightSecond}") Integer weight,
                                   @Value("${italian.arabica.name}") String name) {
        return new Arabica(italianCountry, name, weight);
    }

    @Bean
    public Arabica italianArabica1(@Value("${italian.arabica.weight}") Integer weight,
                                   @Value("${italian.arabica.name}") String name) {
        return new Arabica(italianCountry, name, weight);
    }

    @Bean
    public Liberica italianLiberica1(@Value("${italian.liberica.weight}") Integer weight,
                                     @Value("${italian.liberica.name}") String name) {
        return new Liberica(italianCountry, name, weight);
    }

    @Bean
    public Liberica italianLiberica2(@Value("${italian.liberica.weightSecond}") Integer weight,
                                     @Value("${italian.liberica.name}") String name) {
        return new Liberica(italianCountry, name, weight);
    }

    @Bean
    public Robusta italianRobusta1(@Value("${italian.robusta.weight}") Integer weight,
                                   @Value("${italian.robusta.name}") String name) {
        return new Robusta(italianCountry, name, weight);
    }

    @Bean
    public Robusta italianRobusta2(@Value("${italian.robusta.weightSecond}") Integer weight,
                                   @Value("${italian.robusta.name}") String name) {
        return new Robusta(italianCountry, name, weight);
    }

    @Bean
    public Arabica spanishArabica2(@Value("${spanish.arabica.weightSecond}") Integer weight,
                                   @Value("${spanish.arabica.name}") String name) {
        return new Arabica(spanishCountry, name, weight);
    }

    @Bean
    public Arabica spanishArabica1(@Value("${spanish.arabica.weight}") Integer weight,
                                   @Value("${spanish.arabica.name}") String name) {
        return new Arabica(spanishCountry, name, weight);
    }

    @Bean
    public Liberica spanishLiberica1(@Value("${spanish.liberica.weight}") Integer weight,
                                     @Value("${spanish.liberica.name}") String name) {
        return new Liberica(spanishCountry, name, weight);
    }

    @Bean
    public Liberica spanishLiberica2(@Value("${spanish.liberica.weightSecond}") Integer weight,
                                     @Value("${spanish.liberica.name}") String name) {
        return new Liberica(spanishCountry, name, weight);
    }

    @Bean
    public Robusta spanishRobusta1(@Value("${spanish.robusta.weight}") Integer weight,
                                   @Value("${spanish.robusta.name}") String name) {
        return new Robusta(spanishCountry, name, weight);
    }

    @Bean
    public Robusta spanishRobusta2(@Value("${spanish.robusta.weightSecond}") Integer weight,
                                   @Value("${spanish.robusta.name}") String name) {
        return new Robusta(spanishCountry, name, weight);
    }

    @Bean
    public Arabica germanArabica2(@Value("${german.arabica.weightSecond}") Integer weight,
                                  @Value("${german.arabica.name}") String name) {
        return new Arabica(germanCountry, name, weight);
    }

    @Bean
    public Arabica germanArabica1(@Value("${german.arabica.weight}") Integer weight,
                                  @Value("${german.arabica.name}") String name) {
        return new Arabica(germanCountry, name, weight);
    }

    @Bean
    public Liberica germanLiberica1(@Value("${german.liberica.weight}") Integer weight,
                                    @Value("${german.liberica.name}") String name) {
        return new Liberica(germanCountry, name, weight);
    }

    @Bean
    public Liberica germanLiberica2(@Value("${german.liberica.weightSecond}") Integer weight,
                                    @Value("${german.liberica.name}") String name) {
        return new Liberica(germanCountry, name, weight);
    }

    @Bean
    public Robusta germanRobusta1(@Value("${german.robusta.weight}") Integer weight,
                                  @Value("${german.robusta.name}") String name) {
        return new Robusta(germanCountry, name, weight);
    }

    @Bean
    public Robusta germanRobusta2(@Value("${german.robusta.weightSecond}") Integer weight,
                                  @Value("${german.robusta.name}") String name) {
        return new Robusta(germanCountry, name, weight);
    }

}
