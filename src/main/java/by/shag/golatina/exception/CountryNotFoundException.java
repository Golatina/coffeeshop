package by.shag.golatina.exception;

public class CountryNotFoundException extends RuntimeException {

    public CountryNotFoundException(String country) {
        super("No representation in the " + country);
    }


}
