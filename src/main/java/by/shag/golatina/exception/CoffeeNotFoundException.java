package by.shag.golatina.exception;

public class CoffeeNotFoundException extends RuntimeException {

    public CoffeeNotFoundException(String className, String product) {
        super("Coffee " + product + " not found. " + className);
    }

}
